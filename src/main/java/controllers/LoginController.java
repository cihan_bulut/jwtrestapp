package controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import tokencomponent.TokenBean;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cihan on 19.11.2016.
 */
@Controller
@RequestMapping("/")
public class LoginController {

    @RequestMapping(value = "login")
    public String getLoginPage(Model model) {
        final String uri = "https://pi.uyum.me/login";
        String stringHttpEntity = "{\"deviceSerial\":\"b8:27:eb:72:7a:70\",\"deviceLocalIp\":\"192.168.1.74\",\"deviceVersion\":\"1.0.0\"}";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        String responseJson = null;
        try {
            responseJson = restTemplate.postForObject(uri, stringHttpEntity, String.class);
        } catch (RestClientException e) {
            if (e instanceof HttpStatusCodeException) {
                System.out.println(((HttpStatusCodeException) e).getResponseBodyAsString());
            }
            e.printStackTrace();
        }
        TokenBean tokenBean = TokenBean.getTokenBeanInstance();
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, String> jwtMap = new HashMap<>();
        try {
            jwtMap = objectMapper.readValue(responseJson, new TypeReference<HashMap<String, String>>() {
            });
        } catch (Exception e) {

        }
        tokenBean.setJwt(jwtMap.get("jwt"));
        System.out.println(tokenBean.getJwt());
        model.addAttribute("token", tokenBean.getJwt());
        model.addAttribute("json", stringHttpEntity);
        return "login";
    }

}
