package controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Card;
import models.Device;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import tokencomponent.TokenBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cihan on 19.11.2016.
 */
@Controller
public class InitController {
    @RequestMapping(value = "/init")
    public String getInit(Model model){
        TokenBean tokenBean = TokenBean.getTokenBeanInstance();
        if(tokenBean.getJwt() != null) {
            model.addAttribute("token", tokenBean.getJwt());

            List<Card> cards = new ArrayList<>();
            cards.add(new Card("CX1982179837","UyumSoft Bilgi Sistemleri","287623876832",9,"Karpuz kestik yenmi?"));
            cards.add(new Card("CR3287683268","UyumSoft Kurumsal Is Sistemleri","129873987932",0,"PIN Doğru."));
            cards.add(new Card("OS3297329879","BilgeAdam Bilisim Ltd. Sti.","238761828783",1,"Son pin deneme hakkiniz.."));
            Device device = new Device();
            device.setDeviceSerial("b8:27:eb:72:7a:70");
            device.setCards(cards);

            final String uri = "https://pi.uyum.me/init";
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = null;
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode dataTable = mapper.createObjectNode();
            dataTable.put("jwt",tokenBean.getJwt());
            dataTable.put("deviceSerial",device.getDeviceSerial());
            dataTable.putPOJO("cards",device.getCards());

            model.addAttribute("jsonData",dataTable.toString());


            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            String responseJson = null;
            try{
                responseJson = restTemplate.postForObject(uri,dataTable.toString(),String.class);
            }catch (RestClientException e){
                if(e instanceof HttpStatusCodeException){
                    System.out.println(((HttpStatusCodeException)e).getResponseBodyAsString());
                }
            }
            model.addAttribute("reJsonData",responseJson);
        }else{
            return "error";
        }
        return "init";
    }
}
