package tokencomponent;

/**
 * Created by cihan on 20.11.2016.
 */
public class TokenBean {
    private static TokenBean  tokenBean = null;
    private String jwt;
    private TokenBean(){

    }

    public static TokenBean getTokenBeanInstance(){
        synchronized (TokenBean.class){
            if(tokenBean == null){
                tokenBean = new TokenBean();
            }
        }
        return tokenBean;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
