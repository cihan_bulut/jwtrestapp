package models;

import java.util.List;

/**
 * Created by cihan on 19.11.2016.
 */
public class Device {
    private String deviceSerial;
    private List<Card> cards;

    public String getDeviceSerial() {
        return deviceSerial;
    }

    public void setDeviceSerial(String deviceSerial) {
        this.deviceSerial = deviceSerial;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    @Override
    public String toString() {
        return "{" +
                "deviceSerial='" + deviceSerial + '\'' +
                ", cards=" + cards +
                '}';
    }
}
