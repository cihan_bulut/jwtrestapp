package models;

/**
 * Created by cihan on 19.11.2016.
 */
public class Card {
    private String cardReaderSerial;
    private String cardName;
    private String cardVkn;
    private int cardStatus;
    private String statusMessage;

    public Card(String cardReaderSerial, String cardName, String cardVkn, int cardStatus, String statusMessage) {
        this.cardReaderSerial = cardReaderSerial;
        this.cardName = cardName;
        this.cardVkn = cardVkn;
        this.cardStatus = cardStatus;
        this.statusMessage = statusMessage;
    }

    public String getCardReaderSerial() {
        return cardReaderSerial;
    }

    public void setCardReaderSerial(String cardReaderSerial) {
        this.cardReaderSerial = cardReaderSerial;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardVkn() {
        return cardVkn;
    }

    public void setCardVkn(String cardVkn) {
        this.cardVkn = cardVkn;
    }

    public int getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(int cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public String toString() {
        return "{" +
                "\"cardReaderSerial\":\"" + cardReaderSerial + '\"' +
                ", \"cardName\":\"" + cardName + '\"' +
                ", \"cardVkn\":\"" + cardVkn + '\"' +
                ", \"cardStatus\":" + cardStatus +
                ", \"statusMessage\":\"" + statusMessage + '\"' +
                '}';
    }
}
